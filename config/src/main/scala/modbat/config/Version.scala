package modbat.config

class Version(pkg: Package) {
  assert(pkg != null,
    {
      "Cannot find package; available packages: " + Package.getPackages.mkString("\n")
    })

  override def toString(): String = {
    pkg.getSpecificationVersion + " rev " + pkg.getImplementationVersion
  }
}
