package modbat.examples

import org.scalatest._
import modbat.mbt.ModbatTestHarness

class JavaNioServerSocket3Test extends fixture.FlatSpec with fixture.TestDataFixture with Matchers {
 "JavaNioServerSocket3Test1" should "pass" in {
  td =>
  ModbatTestHarness.test(Array("-n=10", "-s=1", "--no-redirect-out", "--loop-limit=5", "modbat.examples.JavaNioServerSocket3"),
  (() => ModbatTestHarness.setExamplesJar()), td)
 }
 "JavaNioServerSocket3Test2" should "pass" in {
  td =>
  ModbatTestHarness.test(Array("-n=100", "-s=1", "--no-redirect-out","--loop-limit=5", "modbat.examples.JavaNioServerSocket3"),
  (() => ModbatTestHarness.setExamplesJar()), td)
 }
 "JavaNioServerSocket3Test3" should "pass" in {
  td =>
  ModbatTestHarness.test(Array("-n=200", "-s=1", "--no-redirect-out","--loop-limit=5", "modbat.examples.JavaNioServerSocket3"),
  (() => ModbatTestHarness.setExamplesJar()), td)
 }
}

