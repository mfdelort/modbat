package modbat.examples

import org.scalatest._
import modbat.mbt.ModbatTestHarness

class CounterModelTest extends fixture.FlatSpec with fixture.TestDataFixture with Matchers {
 "CounterModelTest1" should "pass" in { 
  td =>
  ModbatTestHarness.test(Array("-s=1", "-n=30", "--no-redirect-out", "modbat.examples.CounterModel"),
  (() => ModbatTestHarness.setExamplesJar()), td)
 }

 "CounterModelTest2" should "pass" in { 
  td =>
  ModbatTestHarness.test(Array("-s=1", "-n=30", "--no-redirect-out", "modbat.examples.CounterModel2"),
  (() => ModbatTestHarness.setExamplesJar()), td)
 }
}
