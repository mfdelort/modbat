package modbat.test
import org.scalatest._
import modbat.mbt.ModbatTestHarness

class NoSelfJoinTest extends fixture.FlatSpec with fixture.TestDataFixture with Matchers{
  "NoSelfJoinTest1" should "pass" in{
  td=>
  ModbatTestHarness.test(Array("-s=1","-n=1","--no-redirect-out","--log-level=fine","modbat.test.NoSelfJoin"),
  (()=>ModbatTestHarness.setTestJar()),td)
 }
}


