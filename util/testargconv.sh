#!/bin/sh

shift # run
shift # exit code

/bin/echo -n "Array("
for arg in "$@"
do
    if [[ ("$prev") ]]
    then
        /bin/echo -n "\"$prev\", "
    fi
    prev="$arg"
done
echo "\"$arg\")"

cls=$arg
unset prev

echo
/bin/echo -n "git mv log/modbat/$cls/"
for arg in "$@"
do
    if [[ ("$prev") ]]
    then
        /bin/echo -n "$prev"
    fi
    prev="$arg"
done
/bin/echo -n ".out log/modbat/$cls"
/bin/echo -n "Test/$cls""TestX.out"
echo
echo "# REPLACE X above!"

unset prev
echo
/bin/echo -n "git mv log/modbat/$cls/"
for arg in "$@"
do
    if [[ ("$prev") ]]
    then
        /bin/echo -n "$prev"
    fi
    prev="$arg"
done
/bin/echo -n ".eout log/modbat/$cls"
/bin/echo -n "Test/$cls""TestX.eout"
echo
echo "# REPLACE X above!"
